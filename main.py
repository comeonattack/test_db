import psycopg2
import hashlib

def db_connect():
    """Функция подключения к бд"""
    con = psycopg2.connect(
    database="test_db", 
    user="root", 
    password="root", 
    host="localhost", 
    port="5432"
    )

    print("Database opened successfully")
    return con

def db_query(query, con):
    """Функция отправки запроса к бд"""
    cur = con.cursor() 
    cur.execute(query)
    return cur

def db_select(columns, table, con):
    query = f"SELECT {columns} FROM {table}"
    result = db_query(query, con)
    return result.fetchall()

def db_insert(table_name, columns, values, con):
    query = f"INSERT INTO {table_name} ({columns}) VALUES ({values})"
    db_query(query, con)
    return 0


def authorization(con):
    """Функция авторизации"""
    username = input('Введите логин -->  ')
    password = input('Введите пароль -->  ')

    check = len(username)
    if check > 20:
        print('eblan?')
        return 

    result=db_query(f"SELECT password_hash FROM users WHERE username = '{username}'", con).fetchall()

    hash = hashlib.md5(password.encode()).hexdigest()

    if len(result) == 0:
        print('Такого пользователя не существует')
        print('Хотите создать аккаунт? (Да/Нет)')
        answer = input()
        check_answer = len(answer)
        if check_answer > 3:
            print('eblan?')
        else:
            if answer == 'Да' or answer == 'да':
                registration(con)
            else:
                print('Ну и пиздуй нахуй отсюда')
    elif hash == result[0][0]:
        print('Вы внутри')
    else:
        print('eblan?')
    
    return 0

def registration(con):
    """Функция регистрации"""
    username = input('Введите логин -->  ')
    password = input('Введите пароль -->  ')
    age = int(input('Введите ваш возраст -->  '))

    check_username = len(username)
    check_password = len(password)
    if check_username > 20 and age > 100 and check_password > 20:
        print('eblan?')
        return 
    
    hash = hashlib.md5(password.encode()).hexdigest()

    #result = db_query(f"INSERT INTO users (username, password_hash, age) VALUES ('{username}', '{hash}', {age})", con)
    db_insert("users", "username, password_hash, age", f"'{username}', '{hash}', {age}", con)
    return 0




def main():
    """Основная функция"""
    con = db_connect()

    authorization(con)

    print("Operation done successfully")  

if __name__ == '__main__':
    main() 


